package assignment2;


import org.junit.Assert;
import org.junit.Test;
/**
 * @author Xavier Ngansop
 * HA2: 07/10
 * -2 Test gescheitert
 * -1 Node muss nicht abstract sein
 *
 */
public class GeneralTest {

    @Test
    public void testCompositePattern(){
        Uni uni = new Uni();

        Fakultaet fakultaet = new Fakultaet();

        Fachbereich informatik = new Fachbereich();

        Institut INFinstitutPraxis = new Institut();
        Institut INFinstitutTheorie = new Institut();

        Fachbereich elektrotechnik = new Fachbereich();

        Fachgebiet softwaretechnik = new Fachgebiet(60);
        Fachgebiet theorie = new Fachgebiet(10);
        Fachgebiet datenbanken = new Fachgebiet(30);

        Fachgebiet signaltechnik = new Fachgebiet(24);
        Fachgebiet grundlagenEtechnik = new Fachgebiet(55);
        Fachgebiet regelungstechnik = new Fachgebiet(21);

        Fachgebiet digitalisierung = new Fachgebiet(42);

        Person inf1 = new Person();
        Person inf2 = new Person();
        Person inf3 = new Person();
        Person inf4 = new Person();
        Person inf5 = new Person();
        Person inf6 = new Person();

        Person et1 = new Person();
        Person et2 = new Person();
        Person et3 = new Person();
        Person et4 = new Person();
        Person et5 = new Person();
        Person et6 = new Person();

        regelungstechnik.addChild(et1);
        regelungstechnik.addChild(et2);
        grundlagenEtechnik.addChild(et3);
        grundlagenEtechnik.addChild(et4);
        signaltechnik.addChild(et5);
        signaltechnik.addChild(et6);

        elektrotechnik.addChild(signaltechnik);
        elektrotechnik.addChild(grundlagenEtechnik);
        elektrotechnik.addChild(regelungstechnik);

        fakultaet.addChild(elektrotechnik);



        datenbanken.addChild(inf1);
        datenbanken.addChild(inf2);
        theorie.addChild(inf3);
        theorie.addChild(inf4);
        softwaretechnik.addChild(inf5);
        softwaretechnik.addChild(inf6);

        INFinstitutPraxis.addChild(softwaretechnik);
        INFinstitutPraxis.addChild(datenbanken);
        INFinstitutTheorie.addChild(theorie);

        informatik.addChild(INFinstitutPraxis);
        informatik.addChild(INFinstitutTheorie);

        fakultaet.addChild(informatik);

        uni.addChild(fakultaet);

        uni.addChild(digitalisierung); // There is no 'Präsidium' i suppose you mean 'Uni' in this case

        BudgetVisitor budgetVisitor = new BudgetVisitor();
        uni.accept(budgetVisitor);
        Assert.assertEquals("Integer values are not equal. Uni budget not correctly computed", 242, budgetVisitor.getCurrentBudget());

        budgetVisitor.setCurrentBudget(0);
        fakultaet.accept(budgetVisitor);
        Assert.assertEquals("Integer values are not equal. Informatik budget not correctly computed", 200, budgetVisitor.getCurrentBudget());

        budgetVisitor.setCurrentBudget(0);
        informatik.accept(budgetVisitor);
        Assert.assertEquals("Integer values are not equal. Informatik budget not correctly computed", 100, budgetVisitor.getCurrentBudget());

        budgetVisitor.setCurrentBudget(0);
        INFinstitutPraxis.accept(budgetVisitor);
        Assert.assertEquals("Integer values are not equal. Informatik budget not correctly computed", 90, budgetVisitor.getCurrentBudget());

        budgetVisitor.setCurrentBudget(0);
        INFinstitutTheorie.accept(budgetVisitor);
        Assert.assertEquals("Integer values are not equal. Informatik budget not correctly computed", 10, budgetVisitor.getCurrentBudget());

        budgetVisitor.setCurrentBudget(0);
        elektrotechnik.accept(budgetVisitor);
        Assert.assertEquals("Integer values are not equal. Elektrotechnik budget not correctly computed", 100, budgetVisitor.getCurrentBudget());



    }



}
