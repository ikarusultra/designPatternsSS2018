package assignment3;

import org.junit.Assert;
import org.junit.Test;
/**
 * HA3: 10/10
 */
public class ChainOfResponsibilityTest
{
    String code = "Ldc 6\n" +
            "Ldc 7\n" +
            "Mult\n" +
            "Store x\n" +
            "Ld x\n" +
            "Print\n";

    String code2 = "Ldc 10\n" +
            "Ldc 10\n" +
            "Mult\n";


    @Test
    public void testChain(){
        Interpreter interpreter = new Interpreter();
        interpreter.parse(code);

        Assert.assertEquals("Integer values are not equal. Stack is not empty", 0, interpreter.getStackSize());

        interpreter.parse(code2);

        Assert.assertEquals("Integer values are not equal. Value should be 100.", 100, interpreter.getTopStackElement());
        Assert.assertEquals("Integer values are not equal. Stack size should be 1.", 1, interpreter.getStackSize());
    }
}
