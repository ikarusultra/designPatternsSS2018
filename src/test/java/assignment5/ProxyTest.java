package assignment5;

import assignment5.client.TransporterClientGUI;
import assignment5.server.Service;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

import java.util.ArrayList;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.control.LabeledMatchers.hasText;

/**
 * HA5: 08/10
 * -2 java.util.NoSuchElementException
 * 	at java.util.Optional.orElseThrow(Optional.java:290)
 * 	at org.testfx.service.finder.impl.WindowFinderImpl.window(WindowFinderImpl.java:70)
 * 	at org.testfx.robot.impl.WriteRobotImpl.fetchTargetWindow(WriteRobotImpl.java:86)
 * 	at org.testfx.robot.impl.WriteRobotImpl.write(WriteRobotImpl.java:78)
 * 	at org.testfx.robot.impl.WriteRobotImpl.write(WriteRobotImpl.java:73)
 * 	at org.testfx.api.FxRobot.write(FxRobot.java:506)
 * 	at org.testfx.api.FxRobot.write(FxRobot.java:60)
 * 	at assignment5.ProxyTest.placeOrder(ProxyTest.java:46)
 */
public class ProxyTest extends ApplicationTest {

    Service headlessTrasportService;

    @Before
    public void setup() throws Exception {
        FxToolkit.registerPrimaryStage();
        FxToolkit.setupApplication(TransporterClientGUI.class);
        headlessTrasportService = new Service();
    }


    /*
    Places an order and checks the tranport server delivers it.
     */
    @Test
    public void placeOrder() {
        // expect:
        verifyThat(".button", hasText("Paket beauftragen"));
        ArrayList<Node> allClientLabels = new ArrayList<>();
        allClientLabels.addAll(lookup(".label").queryAll());
        ArrayList<Node> allClientTextfields = new ArrayList<>();
        allClientTextfields.addAll(lookup(".text-field").queryAll());

        clickOn(allClientTextfields.get(0)).write("Niclas");
        clickOn(allClientTextfields.get(1)).write("Frankfurt");
        clickOn(allClientTextfields.get(2)).write("Kassel");

        Assert.assertEquals("Aufgegebene Sendungen: 0", ((Label) allClientLabels.get(0)).getText());

        clickOn("Paket beauftragen");

        Assert.assertEquals(1, ((ListView)(lookup(".list-view").query())).getItems().size());


        moveTruck();
        sleep(1000);

        Assert.assertTrue(((String)((ListView)(lookup(".list-view").query())).getItems().get(0)).contains("delivered"));
    }


    private void moveTruck(){
        headlessTrasportService.driveAndDeliverWithoutGUI();
        headlessTrasportService.driveAndDeliverWithoutGUI();
        headlessTrasportService.driveAndDeliverWithoutGUI();
        headlessTrasportService.driveAndDeliverWithoutGUI();
        headlessTrasportService.driveAndDeliverWithoutGUI();
        headlessTrasportService.driveAndDeliverWithoutGUI();
    }

    @After
    public void teardown() throws Exception {
        FxToolkit.cleanupStages();
        headlessTrasportService = null;
    }


}
