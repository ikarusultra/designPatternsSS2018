package assignment4;

import org.junit.Assert;
import org.junit.Test;
/**
 * HA4: 10/10
 */
public class ListenerTest
{

    @Test
    public void testListener(){
        long startTime = System.currentTimeMillis();

        TimeWriter timeWriter = new TimeWriter();
        FileListener fileListener = new FileListener();
        Thread t1 = new Thread(timeWriter);
        Thread t2 = new Thread(fileListener);

        t1.start();
        t2.start();

        while(System.currentTimeMillis() < startTime + 10000){} //Interrupts both Threads after 10 seconds

        t1.interrupt();
        t2.interrupt();

        Assert.assertTrue(
                "There should be 4 to 6 file changes within 10 seconds. Actual: " + fileListener.getCatchedPropertyChanges(),
                fileListener.getCatchedPropertyChanges() < 7 && fileListener.getCatchedPropertyChanges() > 3
        );

    }
}
