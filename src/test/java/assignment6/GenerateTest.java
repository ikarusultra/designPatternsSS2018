package assignment6;

import de.uniks.networkparser.graph.Cardinality;
import de.uniks.networkparser.graph.Clazz;
import de.uniks.networkparser.graph.DataType;
import org.junit.Test;
import org.sdmlib.models.classes.ClassModel;

public class GenerateTest {

    @Test
    public void generate() {
        ClassModel model = new ClassModel("assignment6.model");

        Clazz group = model.createClazz("Group")
                .withAttribute("name", DataType.STRING);

        Clazz line = model.createClazz("Line")
                .withAttribute("x1", DataType.INT)
                .withAttribute("x2", DataType.INT)
                .withAttribute("y1", DataType.INT)
                .withAttribute("y2", DataType.INT)
                .withAttribute("name", DataType.STRING);

        group.withBidirectional(line, "lines", Cardinality.MANY, "group", Cardinality.ONE);
        group.withBidirectional(group, "groups", Cardinality.MANY, "parentGroup", Cardinality.ONE);

        model.generate();
    }

}
