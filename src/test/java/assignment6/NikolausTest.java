package assignment6;

import javafx.scene.Node;
import javafx.scene.control.TextField;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

import java.util.ArrayList;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.control.LabeledMatchers.hasText;

/**
 * org.sdmlib.models.debug.FlipBook is broken. Undo and Redo implementation does not work.
 */
// HA6: 10/10
public class NikolausTest extends ApplicationTest {
    @Before
    public void setup() throws Exception {
        FxToolkit.registerPrimaryStage();
        FxToolkit.setupApplication(DrawApplication.class);
    }


    /*
    Places an order and checks the transport server delivers it.
     */
    @Test
    public void placeOrder() {
        // expect:
        verifyThat(".button", hasText("Execute Command"));
        ArrayList<Node> allClientTextfields = new ArrayList<>();
        allClientTextfields.addAll(lookup(".text-field").queryAll());

        clickOn(allClientTextfields.get(0)).write("line line1 25 150 125 250");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("line line2 25 250 125 150");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("line line3 25 150 25 250");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("line line4 25 250 125 250");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("line line5 125 250 125 150");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("line line6 25 150 125 150");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("line line7 25 150 75 60");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("line line8 125 150 75 60");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("group group1 line1 line2 line3 line4 line5 line6 line7 line8");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("clone group1 group2 150 0");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("clone group2 group3 150 0");
        clickOn(".button");

        clickOn(allClientTextfields.get(0)).write("undo");
        clickOn(".button");

        sleep(2500);

        Assert.assertTrue(((TextField)allClientTextfields.get(0)).getText().equals(""));
    }


    @After
    public void teardown() throws Exception {
        FxToolkit.cleanupStages();
    }
}
