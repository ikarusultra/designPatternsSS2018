package assignment1;

import org.junit.Test;
/**
 * @author Xavier Ngansop
 * HA1: 08/10
 * -2 Carterer muss eine abstrckte Klasse oder ein Interface
 * Das ist kein Junit-Test es fehlen Assertions. Ich ziehe Punkte dafür nächstes mal
 * Bitte mach ein Package statt ein Projekt für jede Aufgabe
 */
public class DeliveryTest {

    @Test
    public void testDeliveries(){
        DeliveryService service = new DeliveryService();

        service.setSubContractor(new PizzaShop());
        service.getSubContractor().deliver(42, "WA73");

        service.setSubContractor(new BurgerBude());
        service.getSubContractor().deliver(42, "WA73");

        service.setSubContractor(new DoenerLaden());
        service.getSubContractor().deliver(42, "WA73");

        service.setSubContractor(new AsiaImbiss());
        service.getSubContractor().deliver(42, "WA73");

    }
}
