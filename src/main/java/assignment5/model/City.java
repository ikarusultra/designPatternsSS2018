package assignment5.model;


public class City {

    private String name;

    public String getName() {

        return name;
    }

    public City(String name) {

        this.name = name;
    }
}
