package assignment5.model;

public class Customer {

    public Customer(String name, City location) {
        this.name = name;
        this.location = location;
    }

    private String name;

    public City getLocation() {
        return location;
    }

    public void setLocation(City location) {
        this.location = location;
    }

    private City location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
