package assignment5.model;

import java.util.ArrayList;
import java.util.List;

public class Transporter {

    private City currentLocation;

    private List<Shipment> shipments = new ArrayList<>();


    public Transporter() {
        cities.add(new City("Köln"));
        cities.add(new City("Frankfurt"));
        cities.add(new City("Eschwege"));
        cities.add(new City("Kassel"));
        cities.add(new City("Berlin"));
        cities.add(new City("Kiel"));
        currentLocation = getCities().get(0);
    }

    public City moveToNextCity() {
        int position = getCities().indexOf(getCurrentLocation()) + 1;

        if (position < getCities().size()) {
            setCurrentLocation(getCities().get(position));
        } else {
            setCurrentLocation(getCities().get(0));
        }

        return getCurrentLocation();
    }

    private List<City> cities = new ArrayList<>();

    public List<City> getCities() {
        return cities;
    }

    public City getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(City currentLocation) {
        this.currentLocation = currentLocation;
    }

    public List<Shipment> getShipments() {
        return shipments;
    }

    public void addBox(Shipment boxes) {
        this.shipments.add(boxes);
    }
}
