package assignment5.model;

public class Shipment {

    public static final String STATE_WAITING = "waiting for shipments";
    public static final String STATE_ON_THE_WAY = "Shipment on the way";
    public static final String STATE_DELIVERED = "Shipment delivered";

    private String id;

    private String state;

    private City destination;

    private Customer sender;

    public Customer getSender() {
        return sender;
    }

    public void setSender(Customer sender) {
        this.sender = sender;
    }

    public City getDestination() {
        return destination;
    }

    public void setDestination(City destination) {
        this.destination = destination;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Shipment(Customer sender, City destination) {
        this.sender = sender;
        this.destination = destination;

        this.id = sender.getName() + " " + sender.getLocation().getName();
    }

}