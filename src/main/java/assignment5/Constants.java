package assignment5;

public class Constants {
    public static final String CHANNEL_SHIPMENT_STATE_CHANGED = "de/uni/kassel/designpattern/33203230/assignment5/to";
    public static final String CHANNEL_SEND_SHIPMENT = "de/uni/kassel/designpattern/33203230/assignment5/toTransporter";
}
