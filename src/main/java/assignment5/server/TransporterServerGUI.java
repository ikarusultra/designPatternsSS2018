package assignment5.server;

import assignment5.model.City;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class TransporterServerGUI extends Application {

    private Service transportService;
    private Label label;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("TransportServer");
        BorderPane root = new BorderPane();
        label = new Label("");
        ListView<String> cities = new ListView<String>();
        Button btn = new Button();

        transportService = new Service();


        cities.setMouseTransparent(true);
        cities.setOrientation(Orientation.HORIZONTAL);
        for (City city : transportService.getTransporter().getCities()) {
            cities.getItems().add(city.getName());
        }
        cities.getSelectionModel().selectFirst();



        btn.setText("Ausliefern");
        btn.setOnAction(event -> {
            transportService.driveAndDeliver(cities);
            label.setText(transportService.getInfotext());
        });


        BorderPane.setMargin(label, new Insets(230, 5, 5, 5));
        root.setTop(btn);
        root.setCenter(cities);
        root.setRight(label);
        primaryStage.setScene(new Scene(root, 600, 500));
        primaryStage.show();
    }



}
