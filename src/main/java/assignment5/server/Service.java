package assignment5.server;

import assignment5.model.Shipment;
import assignment5.model.City;
import assignment5.model.Transporter;
import com.google.gson.Gson;
import javafx.application.Platform;
import javafx.scene.control.ListView;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static assignment5.Constants.CHANNEL_SEND_SHIPMENT;
import static assignment5.Constants.CHANNEL_SHIPMENT_STATE_CHANGED;


public class Service implements MqttCallback {

    private Transporter transporter = new Transporter();
    private List<Shipment> shipments = new ArrayList<>();
    private MqttClient sampleClient;
    private Gson gson = new Gson();
    private String infotext;



    void updateState(Shipment shipment, String newState) {
        shipment.setState(newState);
        String messageContent = gson.toJson(shipment);
        MqttMessage message = new MqttMessage(messageContent.getBytes());
        message.setQos(2);
        try {
            sampleClient.publish(CHANNEL_SHIPMENT_STATE_CHANGED + shipment.getSender().getName(), message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        String str = new String(message.getPayload(), StandardCharsets.UTF_8);
        System.out.println(str);
        Shipment shipment = gson.fromJson(str, Shipment.class);
        Platform.runLater(() -> {
            shipmentSend(shipment);
        });
    }

    public void driveAndDeliver(ListView<String> cities) {
        collectShipments(transporter.getCurrentLocation());
        transporter.moveToNextCity();
        deliverBoxes(transporter.getCurrentLocation());
        if (cities.getSelectionModel().getSelectedIndex() + 1 < cities.getItems().size()) {
            cities.getSelectionModel().selectNext();
        } else {
            cities.getSelectionModel().selectFirst();
        }
    }

    public void driveAndDeliverWithoutGUI() {
        collectShipments(transporter.getCurrentLocation());
        transporter.moveToNextCity();
        deliverBoxes(transporter.getCurrentLocation());
    }

    private void deliverBoxes(City city) {
        List<Shipment> removedTruckShipments = new ArrayList<>();
        for (Shipment shipment : transporter.getShipments()) {
            if (shipment.getDestination().getName().equals(city.getName())) {
                updateState(shipment, Shipment.STATE_DELIVERED);
                removedTruckShipments.add(shipment);
            }
        }
        transporter.getShipments().removeAll(removedTruckShipments);

        infotext = "Ausstehende Sendungen: " + shipments.size() + " Eingeladene Sendungen: " + transporter.getShipments().size();
    }

    private void collectShipments(City city) {
        // take all shipments from this city
        List<Shipment> removedShipments = new ArrayList<>();
        for (Shipment shipment : shipments) {
            if (shipment.getSender().getLocation().getName().equals(city.getName())) {
                updateState(shipment, Shipment.STATE_ON_THE_WAY);
                transporter.addBox(shipment);
                removedShipments.add(shipment);
            }
        }
        shipments.removeAll(removedShipments);

        infotext = "Ausstehende Sendungen: " + shipments.size() + " Eingeladene Sendungen: " + transporter.getShipments().size();
    }

    public void shipmentSend(Shipment shipment) {
        shipments.add(shipment);

        infotext = "Ausstehende Sendungen: " + shipments.size() + " Eingeladene Sendungen: " + transporter.getShipments().size();
    }

    public Transporter getTransporter() {
        return transporter;
    }

    @Override
    public void connectionLost(Throwable cause) {
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
    }


    public String getInfotext() {
        return infotext;
    }


    public Service() {
        String broker = "tcp://test.mosquitto.org:1883";
        String clientId = "DesignPatternService";
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            sampleClient = new MqttClient(broker, clientId, persistence);
            sampleClient.setCallback(this);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            sampleClient.connect(connOpts);
            sampleClient.subscribe(CHANNEL_SEND_SHIPMENT);
        } catch (MqttException me) {
            me.printStackTrace();
        }
    }



}
