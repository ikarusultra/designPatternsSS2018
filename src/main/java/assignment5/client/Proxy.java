package assignment5.client;

import assignment5.model.Shipment;
import com.google.gson.Gson;
import javafx.application.Platform;
import javafx.scene.control.ListView;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.nio.charset.StandardCharsets;

import static assignment5.Constants.CHANNEL_SEND_SHIPMENT;
import static assignment5.Constants.CHANNEL_SHIPMENT_STATE_CHANGED;

public class Proxy implements MqttCallback{


    private final ListView<String> orders;
    private MqttClient sampleClient;
    private Gson gson = new Gson();

    void sendOrder(Shipment shipment) {
        if (sampleClient== null || !sampleClient.isConnected()) {
            connectClient(shipment.getSender().getName());
        }

        String messageContent = gson.toJson(shipment);
        int qos = 2;
        MqttMessage message = new MqttMessage(messageContent.getBytes());
        message.setQos(qos);

        try {
            sampleClient.publish(CHANNEL_SEND_SHIPMENT, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        String str = new String(message.getPayload(), StandardCharsets.UTF_8);

        Shipment shipment = gson.fromJson(str, Shipment.class);

        Platform.runLater(() -> {
            shipmentStatusChanged(shipment);
        });
    }


    private void connectClient(String sender) {
        String mosquittoServer = "tcp://test.mosquitto.org:1883";
        String clientId = "DesignPatternProxy" + sender;
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            sampleClient = new MqttClient(mosquittoServer, clientId, persistence);
            sampleClient.setCallback(this);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            sampleClient.connect(connOpts);
            sampleClient.subscribe(CHANNEL_SHIPMENT_STATE_CHANGED + sender);

        } catch (MqttException me) {
            me.printStackTrace();
        }
    }

    public void shipmentStatusChanged(Shipment shipment) {
        orders.getItems().removeIf(string -> string.contains(shipment.getId()));

        orders.getItems().add("Sending Shipment: " + shipment.getId() + " " + shipment.getSender().getLocation().getName()
                + " to " + shipment.getDestination().getName() + "Status: " + shipment.getState());
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        //not implemented
    }

    @Override
    public void connectionLost(Throwable cause) {
        //not implemented
    }


    Proxy(ListView<String> towns) {
        this.orders = towns;
    }

}
