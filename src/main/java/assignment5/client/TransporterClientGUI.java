package assignment5.client;


import assignment5.model.Shipment;
import assignment5.model.City;
import assignment5.model.Customer;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class TransporterClientGUI extends Application {

    private List<Shipment> shipments = new ArrayList<>();
    private ListView<String> orders;
    private String customerIdentifier;


    @Override
    public void start(Stage primaryStage) {
        Label infoLabel = new Label("Aufgegebene Sendungen: 0");
        primaryStage.setTitle("TransportServer");

        orders = new ListView<String>();

        Proxy proxy = new Proxy(orders);

        orders.setMaxHeight(280);
        orders.setMouseTransparent(true);
        orders.setOrientation(Orientation.VERTICAL);
        orders.getSelectionModel().selectFirst();

        TextField customerName = new TextField();
        customerName.setMaxWidth(100);
        customerName.setPromptText("Auftraggeber");
        TextField location = new TextField();
        location.setMaxWidth(100);
        location.setPromptText("Start");

        HBox inputs = new HBox(10);
        inputs.setPrefWidth(500);
        HBox.setHgrow(customerName, Priority.ALWAYS);
        HBox.setHgrow(location, Priority.ALWAYS);
        inputs.getChildren().addAll(customerName, location);


        TextField destination = new TextField();
        destination.setMaxWidth(100);
        destination.setPromptText("Ziel");

        Button btn = new Button();
        HBox inputDestination = new HBox(10);
        inputDestination.setPrefWidth(500);
        HBox.setHgrow(destination, Priority.ALWAYS);
        inputDestination.getChildren().addAll(destination, btn);

        btn.setText("Paket beauftragen");
        btn.setOnAction(event -> {
            if (customerName.getText().equals("") || location.getText().equals("") || destination.getText().equals("")) {
                return;
            }

            customerIdentifier = customerName.getText();

            Customer customer = new Customer(customerIdentifier, new City(location.getText()));
            Shipment shipment = new Shipment(customer, new City(destination.getText()));

            shipments.add(shipment);
            proxy.sendOrder(shipment);
            destination.clear();

            orders.getItems().add("Auftrag erteilt: " + shipment.getId() + " " + shipment.getSender().getLocation().getName()
                    + " nach " + shipment.getDestination().getName());
            infoLabel.setText("Aufgegebene Sendungen: " + shipments.size());
        });

        VBox topInputs = new VBox(50);
        topInputs.getChildren().addAll(inputs, inputDestination);

        BorderPane root = new BorderPane();
        BorderPane.setMargin(infoLabel, new Insets(230, 5, 5, 5));
        root.setTop(topInputs);
        root.setCenter(orders);
        root.setRight(infoLabel);
        primaryStage.setScene(new Scene(root, 660, 550));
        primaryStage.show();
        inputs.requestFocus();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
