package assignment6;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class GUI {

    private Stage primaryStage;
    private Controller controller;

    private final GraphicsContext graphicsContext;


    public GUI(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.controller = new Controller(this);
        primaryStage.setTitle("DrawApplication");
        BorderPane root = new BorderPane();
        HBox hBox = new HBox();
        TextField commandInput = new TextField();
        commandInput.setMinWidth(250);
        //commandInput.setText("line line5 25 25 125 125");

        Pane canvasPane = new Pane();
        Canvas canvas = new Canvas(600, 400);
        graphicsContext = canvas.getGraphicsContext2D();
        canvasPane.setStyle("-fx-padding: 5;" +
                "-fx-border-width: 3;" +
                "-fx-border-insets: 3;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: red;"
        );

        Button btn = new Button();

        btn.setText("Execute Command");
        btn.setOnAction(event -> {
            boolean parsed = controller.parseCommand(commandInput.getText());
            if (parsed){
                commandInput.clear();
            }
        });

        hBox.getChildren().addAll(commandInput, btn);
        canvasPane.getChildren().addAll(canvas);

        BorderPane.setMargin(hBox, new Insets(10, 10, 10, 100));
        root.setBottom(hBox);
        root.setCenter(canvasPane);
        primaryStage.setScene(new Scene(root, 600, 500));
    }

    public void show() {

        primaryStage.show();
    }

    public GraphicsContext getGraphicsContext() {
        return graphicsContext;
    }


}
