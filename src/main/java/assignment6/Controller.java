package assignment6;

import assignment6.commandParser.CommandInterpreter;


public class Controller  {

    public GUI getGui() {
        return gui;
    }

    private GUI gui;
    private CommandInterpreter interpreter;

    public Controller(GUI gui) {
        this.gui = gui;
        this.interpreter = new CommandInterpreter(this);
    }

    public boolean parseCommand(String command) {
        return interpreter.handleCommand(command);
    }
}
