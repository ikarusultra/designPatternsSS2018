package assignment6.commandParser;


public class LineCommandHandler implements CommandHandler {

    @Override
    public boolean handle(String command, CommandInterpreter interpreter) {
        boolean handled = false;
        if (command.startsWith("line ")) {
            String argumentString = command.split("line ")[1];
            String[] arguments = argumentString.split(" ");
            if (arguments.length == 5) {
                interpreter.getTopLevelGroup().createLines()
                        .withName(arguments[0])
                        .withX1(Integer.parseInt(arguments[1]))
                        .withY1(Integer.parseInt(arguments[2]))
                        .withX2(Integer.parseInt(arguments[3]))
                        .withY2(Integer.parseInt(arguments[4]));
                System.out.println("Created Line: " + arguments[0]);
                handled = true;
            } else {
                System.err.println("Wrong command syntax for " + "'line'");
            }
        }
        return handled;
    }
}
