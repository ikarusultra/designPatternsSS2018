package assignment6.commandParser;

import assignment6.model.Group;
import assignment6.model.Line;


public class CloneCommandHandler implements CommandHandler {
    @Override
    public boolean handle(String command, CommandInterpreter interpreter) {
        boolean handled = false;
        if (command.startsWith("clone ")) {
            String argumentString = command.split("clone ")[1];
            String[] arguments = argumentString.split(" ");
            if (arguments.length == 4) {

                for (Group group : interpreter.getTopLevelGroup().getGroups()) {

                    if (group.getName().equals(arguments[0])) {
                        Group clonedGroup = cloneGroup(group);

                        clonedGroup.setName(arguments[1]);

                        addOffset(clonedGroup, Integer.valueOf(arguments[2]), Integer.valueOf(arguments[3]));

                        interpreter.getTopLevelGroup().withGroups(clonedGroup);
                        break;
                    }
                }
                System.out.println("Cloned group " + arguments[0] + " with new name: " + arguments[1]);
                handled = true;
            } else {
                System.err.println("Wrong command syntax for " + "'clone'");
            }
        }
        return handled;
    }
    private Group cloneGroup(Group group) {
        Group clonedGroup = new Group()
                .withName(group.getName());

        for (Group subGroup : group.getGroups()) {
            clonedGroup.withGroups(cloneGroup(subGroup));
        }

        for (Line line : group.getLines()) {
            clonedGroup.withLines(
                    new Line()
                            .withName(line.getName())
                            .withX1(line.getX1())
                            .withY1(line.getY1())
                            .withX2(line.getX2())
                            .withY2(line.getY2())
            );
        }

        return clonedGroup;
    }

    private void addOffset(Group group, int x, int y) {
        for (Group subGroup : group.getGroups()) {
            addOffset(subGroup , x, y);
        }

        for (Line line : group.getLines()) {
            line.withX1(line.getX1() + x)
                    .withY1(line.getY1() + y)
                    .withX2(line.getX2() + x)
                    .withY2(line.getY2() + y);
        }
    }
}
