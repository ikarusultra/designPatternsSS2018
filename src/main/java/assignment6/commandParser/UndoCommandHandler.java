package assignment6.commandParser;

public class UndoCommandHandler implements CommandHandler {
    private CommandInterpreter interpreter;

    @Override
    public boolean handle(String command, CommandInterpreter interpreter) {
        this.interpreter = interpreter;
        boolean handled = false;
        if (command.startsWith("undo")) {
            String[] argumentString = command.split("undo");
            if (argumentString.length == 0) {
                if (interpreter.getStepsPerCommandHistory().size() <= 0) {
                    return false;
                }
                interpreter.getStepsPerCommandFuture().add(interpreter.getFlipBook().currentStep);
                interpreter.getStepsPerCommandHistory().remove(interpreter.getStepsPerCommandHistory().size() - 1);
                long lastStep = interpreter.getStepsPerCommandHistory().get(interpreter.getStepsPerCommandHistory().size() - 1);
                interpreter.getStepsPerCommandHistory().remove(interpreter.getStepsPerCommandHistory().size() - 1);
                interpreter.getFlipBook().back(interpreter.getFlipBook().currentStep - lastStep);

                handled = true;
            }else {
                System.err.println("Wrong command syntax for " + "'undo'");
            }
        }
        return handled;
    }

}
