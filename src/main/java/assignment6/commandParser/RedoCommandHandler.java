package assignment6.commandParser;

public class RedoCommandHandler implements CommandHandler {

    private CommandInterpreter interpreter;

    @Override
    public boolean handle(String command, CommandInterpreter interpreter) {
        this.interpreter = interpreter;
        boolean handled = false;
        if (command.startsWith("redo")) {
            String[] argumentString = command.split("redo");
            if (argumentString.length == 0) {
                if (interpreter.getStepsPerCommandFuture().size() <= 0) {
                    return false;
                }
                interpreter.getStepsPerCommandHistory().add(interpreter.getFlipBook().currentStep);
                long nextStep = interpreter.getStepsPerCommandFuture().get(interpreter.getStepsPerCommandFuture().size() - 1);
                interpreter.getStepsPerCommandFuture().remove(interpreter.getStepsPerCommandFuture().size() - 1);
                interpreter.getFlipBook().forward(nextStep - interpreter.getFlipBook().currentStep);
                handled = true;
            }else {
                System.err.println("Wrong command syntax for " + "'redo'");
            }
        }
        return handled;
    }

}
