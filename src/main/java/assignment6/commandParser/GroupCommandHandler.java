package assignment6.commandParser;

import assignment6.model.Group;
import assignment6.model.Line;

public class GroupCommandHandler implements CommandHandler {
    @Override
    public boolean handle(String command, CommandInterpreter interpreter) {
        boolean handled = false;
        if (command.startsWith("group ")) {
            String argumentString = command.split("group ")[1];
            String[] arguments = argumentString.split(" ");
            if (arguments.length > 1) {
                Group group = new Group()
                        .withName(arguments[0]);

                for (int i = 1; i < arguments.length; i++) {
                    String parameter = arguments[i];
                    boolean parameterHandled = false;

                    for (Line line : interpreter.getTopLevelGroup().getLines()) {
                        if (line.getName().equals(parameter)) {
                            group.withLines(line);
                            interpreter.getTopLevelGroup().withoutLines(line);
                            parameterHandled = true;
                            break;
                        }
                    }

                    if (parameterHandled) {
                        continue;
                    }

                    for (Group subGroup : interpreter.getTopLevelGroup().getGroups()) {
                        if (subGroup.getName().equals(parameter)) {
                            group.withGroups(subGroup);
                            interpreter.getTopLevelGroup().withoutGroups(subGroup);
                        }
                    }
                }
                interpreter.getTopLevelGroup().withGroups(group);
                System.out.println("Created group: " + arguments[0]);
                handled = true;
            } else {
                System.err.println("Wrong command syntax for " + "'group'");
            }
        }
        return handled;
    }
}
