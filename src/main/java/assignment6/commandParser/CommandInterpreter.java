package assignment6.commandParser;

import assignment6.Controller;
import assignment6.model.Group;
import assignment6.model.Line;
import assignment6.model.util.GroupCreator;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import org.sdmlib.models.debug.FlipBook;
import org.sdmlib.serialization.SDMLibJsonIdMap;

import java.util.ArrayList;

public class CommandInterpreter {
    private Controller controller;
    private ArrayList<CommandHandler> handlers;

    private final FlipBook flipBook;
    private Group topLevelGroup = new Group();

    private ArrayList<Long> stepsPerCommandHistory = new ArrayList<>();
    private ArrayList<Long> stepsPerCommandFuture = new ArrayList<>();

    public CommandInterpreter(Controller controller){
        handlers.add(new LineCommandHandler());
        handlers.add(new CloneCommandHandler());
        handlers.add(new RedoCommandHandler());
        handlers.add(new GroupCommandHandler());
        handlers.add(new UndoCommandHandler());
        handlers = new ArrayList<>();
        this.controller = controller;
        SDMLibJsonIdMap idMap = (SDMLibJsonIdMap) GroupCreator.createIdMap("map");
        flipBook = idMap.createFlipBook();
        idMap.getId(topLevelGroup);
    }

    public boolean handleCommand(String command) {
        if (!command.contains("redo")) {
            stepsPerCommandHistory.add(flipBook.currentStep);
            stepsPerCommandFuture.clear();
        }
        boolean handled = false;
        command = command.trim();

        for (CommandHandler handler : handlers){
            handled = handler.handle(command, this);
            if (handled){
                drawCurrentDataModel();
                return true;
            }
        }

        System.err.println("No matching handler found for command: " + command);
        return handled;
    }

    private void drawCurrentDataModel() {
        for (Line line : topLevelGroup.getGroups().getLines()) {
            Platform.runLater(() -> {
                controller.getGui().getGraphicsContext().setStroke(Color.BLACK);
                controller.getGui().getGraphicsContext().setLineWidth(3);
                controller.getGui().getGraphicsContext().strokeLine(line.getX1(),
                        line.getY1(),
                        line.getX2(),
                        line.getY2());
            });
        }
        for (Line line : topLevelGroup.getLines()) {
            Platform.runLater(() -> {
                controller.getGui().getGraphicsContext().setStroke(Color.BLACK);
                controller.getGui().getGraphicsContext().setLineWidth(3);
                controller.getGui().getGraphicsContext().strokeLine(line.getX1(),
                        line.getY1(),
                        line.getX2(),
                        line.getY2());
            });
        }
    }

    public Group getTopLevelGroup() {

        return topLevelGroup;
    }
    public FlipBook getFlipBook() {

        return flipBook;
    }
    public ArrayList<Long> getStepsPerCommandHistory() {
        return stepsPerCommandHistory;
    }
    public ArrayList<Long> getStepsPerCommandFuture() {
        return stepsPerCommandFuture;
    }
}
