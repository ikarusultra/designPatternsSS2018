package assignment6.commandParser;

public interface CommandHandler {
    boolean handle(String command, CommandInterpreter interpreter);
}

