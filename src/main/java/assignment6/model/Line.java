/*
   Copyright (c) 2018 Ikarus
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package assignment6.model;

import de.uniks.networkparser.interfaces.SendableEntity;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyChangeListener;
import de.uniks.networkparser.EntityUtil;
import assignment6.model.Group;

public  class Line implements SendableEntity
{

   
   //==========================================================================
   
   protected PropertyChangeSupport listeners = null;
   
   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null) {
   		listeners.firePropertyChange(propertyName, oldValue, newValue);
   		return true;
   	}
   	return false;
   }
   
   public boolean addPropertyChangeListener(PropertyChangeListener listener) 
   {
   	if (listeners == null) {
   		listeners = new PropertyChangeSupport(this);
   	}
   	listeners.addPropertyChangeListener(listener);
   	return true;
   }
   
   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
   	if (listeners == null) {
   		listeners = new PropertyChangeSupport(this);
   	}
   	listeners.addPropertyChangeListener(propertyName, listener);
   	return true;
   }
   
   public boolean removePropertyChangeListener(PropertyChangeListener listener) {
   	if (listeners != null) {
   		listeners.removePropertyChangeListener(listener);
   	}
   	return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener) {
   	if (listeners != null) {
   		listeners.removePropertyChangeListener(propertyName, listener);
   	}
   	return true;
   }

   
   //==========================================================================
   
   
   public void removeYou()
   {
      setGroup(null);
      firePropertyChange("REMOVE_YOU", this, null);
   }

   
   //==========================================================================
   
   public static final String PROPERTY_NAME = "name";
   
   private String name;

   public String getName()
   {
      return this.name;
   }
   
   public void setName(String value)
   {
      if ( ! EntityUtil.stringEquals(this.name, value)) {
      
         String oldValue = this.name;
         this.name = value;
         this.firePropertyChange(PROPERTY_NAME, oldValue, value);
      }
   }
   
   public Line withName(String value)
   {
      setName(value);
      return this;
   } 


   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();
      
      result.append(" ").append(this.getName());
      result.append(" ").append(this.getX1());
      result.append(" ").append(this.getX2());
      result.append(" ").append(this.getY1());
      result.append(" ").append(this.getY2());
      return result.substring(1);
   }


   
   //==========================================================================
   
   public static final String PROPERTY_X1 = "x1";
   
   private int x1;

   public int getX1()
   {
      return this.x1;
   }
   
   public void setX1(int value)
   {
      if (this.x1 != value) {
      
         int oldValue = this.x1;
         this.x1 = value;
         this.firePropertyChange(PROPERTY_X1, oldValue, value);
      }
   }
   
   public Line withX1(int value)
   {
      setX1(value);
      return this;
   } 

   
   //==========================================================================
   
   public static final String PROPERTY_X2 = "x2";
   
   private int x2;

   public int getX2()
   {
      return this.x2;
   }
   
   public void setX2(int value)
   {
      if (this.x2 != value) {
      
         int oldValue = this.x2;
         this.x2 = value;
         this.firePropertyChange(PROPERTY_X2, oldValue, value);
      }
   }
   
   public Line withX2(int value)
   {
      setX2(value);
      return this;
   } 

   
   //==========================================================================
   
   public static final String PROPERTY_Y1 = "y1";
   
   private int y1;

   public int getY1()
   {
      return this.y1;
   }
   
   public void setY1(int value)
   {
      if (this.y1 != value) {
      
         int oldValue = this.y1;
         this.y1 = value;
         this.firePropertyChange(PROPERTY_Y1, oldValue, value);
      }
   }
   
   public Line withY1(int value)
   {
      setY1(value);
      return this;
   } 

   
   //==========================================================================
   
   public static final String PROPERTY_Y2 = "y2";
   
   private int y2;

   public int getY2()
   {
      return this.y2;
   }
   
   public void setY2(int value)
   {
      if (this.y2 != value) {
      
         int oldValue = this.y2;
         this.y2 = value;
         this.firePropertyChange(PROPERTY_Y2, oldValue, value);
      }
   }
   
   public Line withY2(int value)
   {
      setY2(value);
      return this;
   } 

   
   /********************************************************************
    * <pre>
    *              many                       one
    * Line ----------------------------------- Group
    *              lines                   group
    * </pre>
    */
   
   public static final String PROPERTY_GROUP = "group";

   private Group group = null;

   public Group getGroup()
   {
      return this.group;
   }

   public boolean setGroup(Group value)
   {
      boolean changed = false;
      
      if (this.group != value)
      {
         Group oldValue = this.group;
         
         if (this.group != null)
         {
            this.group = null;
            oldValue.withoutLines(this);
         }
         
         this.group = value;
         
         if (value != null)
         {
            value.withLines(this);
         }
         
         firePropertyChange(PROPERTY_GROUP, oldValue, value);
         changed = true;
      }
      
      return changed;
   }

   public Line withGroup(Group value)
   {
      setGroup(value);
      return this;
   } 

   public Group createGroup()
   {
      Group value = new Group();
      withGroup(value);
      return value;
   } 
}
