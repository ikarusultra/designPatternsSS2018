/*
   Copyright (c) 2018 Ikarus
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package assignment6.model.util;

import de.uniks.networkparser.list.SimpleSet;
import assignment6.model.Group;
import de.uniks.networkparser.interfaces.Condition;
import java.util.Collection;
import de.uniks.networkparser.list.ObjectSet;
import assignment6.model.util.GroupSet;
import java.util.Collections;
import assignment6.model.util.LineSet;
import assignment6.model.Line;

public class GroupSet extends SimpleSet<Group>
{
	public Class<?> getTypClass() {
		return Group.class;
	}

   public GroupSet()
   {
      // empty
   }

   public GroupSet(Group... objects)
   {
      for (Group obj : objects)
      {
         this.add(obj);
      }
   }

   public GroupSet(Collection<Group> objects)
   {
      this.addAll(objects);
   }

   public static final GroupSet EMPTY_SET = new GroupSet().withFlag(GroupSet.READONLY);


   public GroupPO createGroupPO()
   {
      return new GroupPO(this.toArray(new Group[this.size()]));
   }


   public String getEntryType()
   {
      return "assignment6.model.Group";
   }


   @Override
   public GroupSet getNewList(boolean keyValue)
   {
      return new GroupSet();
   }


   @SuppressWarnings("unchecked")
   public GroupSet with(Object value)
   {
      if (value == null)
      {
         return this;
      }
      else if (value instanceof java.util.Collection)
      {
         this.addAll((Collection<Group>)value);
      }
      else if (value != null)
      {
         this.add((Group) value);
      }
      
      return this;
   }
   
   public GroupSet without(Group value)
   {
      this.remove(value);
      return this;
   }


   /**
    * Loop through the current set of Group objects and collect a list of the name attribute values. 
    * 
    * @return List of String objects reachable via name attribute
    */
   public ObjectSet getName()
   {
      ObjectSet result = new ObjectSet();
      
      for (Group obj : this)
      {
         result.add(obj.getName());
      }
      
      return result;
   }


   /**
    * Loop through the current set of Group objects and collect those Group objects where the name attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of Group objects that match the parameter
    */
   public GroupSet createNameCondition(String value)
   {
      GroupSet result = new GroupSet();
      
      for (Group obj : this)
      {
         if (value.equals(obj.getName()))
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Group objects and collect those Group objects where the name attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of Group objects that match the parameter
    */
   public GroupSet createNameCondition(String lower, String upper)
   {
      GroupSet result = new GroupSet();
      
      for (Group obj : this)
      {
         if (lower.compareTo(obj.getName()) <= 0 && obj.getName().compareTo(upper) <= 0)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Group objects and assign value to the name attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of Group objects now with new attribute values.
    */
   public GroupSet withName(String value)
   {
      for (Group obj : this)
      {
         obj.setName(value);
      }
      
      return this;
   }

   /**
    * Loop through the current set of Group objects and collect a set of the Group objects reached via parentGroup. 
    * 
    * @return Set of Group objects reachable via parentGroup
    */
   public GroupSet getParentGroup()
   {
      GroupSet result = new GroupSet();
      
      for (Group obj : this)
      {
         result.with(obj.getParentGroup());
      }
      
      return result;
   }

   /**
    * Loop through the current set of Group objects and collect all contained objects with reference parentGroup pointing to the object passed as parameter. 
    * 
    * @param value The object required as parentGroup neighbor of the collected results. 
    * 
    * @return Set of Group objects referring to value via parentGroup
    */
   public GroupSet filterParentGroup(Object value)
   {
      ObjectSet neighbors = new ObjectSet();

      if (value instanceof Collection)
      {
         neighbors.addAll((Collection<?>) value);
      }
      else
      {
         neighbors.add(value);
      }
      
      GroupSet answer = new GroupSet();
      
      for (Group obj : this)
      {
         if (neighbors.contains(obj.getParentGroup()) || (neighbors.isEmpty() && obj.getParentGroup() == null))
         {
            answer.add(obj);
         }
      }
      
      return answer;
   }

   /**
    * Follow parentGroup reference zero or more times and collect all reachable objects. Detect cycles and deal with them. 
    * 
    * @return Set of Group objects reachable via parentGroup transitively (including the start set)
    */
   public GroupSet getParentGroupTransitive()
   {
      GroupSet todo = new GroupSet().with(this);
      
      GroupSet result = new GroupSet();
      
      while ( ! todo.isEmpty())
      {
         Group current = todo.first();
         
         todo.remove(current);
         
         if ( ! result.contains(current))
         {
            result.add(current);
            
            if ( ! result.contains(current.getParentGroup()))
            {
               todo.with(current.getParentGroup());
            }
         }
      }
      
      return result;
   }

   /**
    * Loop through current set of ModelType objects and attach the Group object passed as parameter to the ParentGroup attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now with the new neighbor attached to their ParentGroup attributes.
    */
   public GroupSet withParentGroup(Group value)
   {
      for (Group obj : this)
      {
         obj.withParentGroup(value);
      }
      
      return this;
   }

   /**
    * Loop through the current set of Group objects and collect a set of the Group objects reached via groups. 
    * 
    * @return Set of Group objects reachable via groups
    */
   public GroupSet getGroups()
   {
      GroupSet result = new GroupSet();
      
      for (Group obj : this)
      {
         result.with(obj.getGroups());
      }
      
      return result;
   }

   /**
    * Loop through the current set of Group objects and collect all contained objects with reference groups pointing to the object passed as parameter. 
    * 
    * @param value The object required as groups neighbor of the collected results. 
    * 
    * @return Set of Group objects referring to value via groups
    */
   public GroupSet filterGroups(Object value)
   {
      ObjectSet neighbors = new ObjectSet();

      if (value instanceof Collection)
      {
         neighbors.addAll((Collection<?>) value);
      }
      else
      {
         neighbors.add(value);
      }
      
      GroupSet answer = new GroupSet();
      
      for (Group obj : this)
      {
         if ( ! Collections.disjoint(neighbors, obj.getGroups()))
         {
            answer.add(obj);
         }
      }
      
      return answer;
   }

   /**
    * Follow groups reference zero or more times and collect all reachable objects. Detect cycles and deal with them. 
    * 
    * @return Set of Group objects reachable via groups transitively (including the start set)
    */
   public GroupSet getGroupsTransitive()
   {
      GroupSet todo = new GroupSet().with(this);
      
      GroupSet result = new GroupSet();
      
      while ( ! todo.isEmpty())
      {
         Group current = todo.first();
         
         todo.remove(current);
         
         if ( ! result.contains(current))
         {
            result.add(current);
            
            todo.with(current.getGroups()).minus(result);
         }
      }
      
      return result;
   }

   /**
    * Loop through current set of ModelType objects and attach the Group object passed as parameter to the Groups attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now with the new neighbor attached to their Groups attributes.
    */
   public GroupSet withGroups(Group value)
   {
      for (Group obj : this)
      {
         obj.withGroups(value);
      }
      
      return this;
   }

   /**
    * Loop through current set of ModelType objects and remove the Group object passed as parameter from the Groups attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now without the old neighbor.
    */
   public GroupSet withoutGroups(Group value)
   {
      for (Group obj : this)
      {
         obj.withoutGroups(value);
      }
      
      return this;
   }

   /**
    * Loop through the current set of Group objects and collect a set of the Line objects reached via lines. 
    * 
    * @return Set of Line objects reachable via lines
    */
   public LineSet getLines()
   {
      LineSet result = new LineSet();
      
      for (Group obj : this)
      {
         result.with(obj.getLines());
      }
      
      return result;
   }

   /**
    * Loop through the current set of Group objects and collect all contained objects with reference lines pointing to the object passed as parameter. 
    * 
    * @param value The object required as lines neighbor of the collected results. 
    * 
    * @return Set of Line objects referring to value via lines
    */
   public GroupSet filterLines(Object value)
   {
      ObjectSet neighbors = new ObjectSet();

      if (value instanceof Collection)
      {
         neighbors.addAll((Collection<?>) value);
      }
      else
      {
         neighbors.add(value);
      }
      
      GroupSet answer = new GroupSet();
      
      for (Group obj : this)
      {
         if ( ! Collections.disjoint(neighbors, obj.getLines()))
         {
            answer.add(obj);
         }
      }
      
      return answer;
   }

   /**
    * Loop through current set of ModelType objects and attach the Group object passed as parameter to the Lines attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now with the new neighbor attached to their Lines attributes.
    */
   public GroupSet withLines(Line value)
   {
      for (Group obj : this)
      {
         obj.withLines(value);
      }
      
      return this;
   }

   /**
    * Loop through current set of ModelType objects and remove the Group object passed as parameter from the Lines attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now without the old neighbor.
    */
   public GroupSet withoutLines(Line value)
   {
      for (Group obj : this)
      {
         obj.withoutLines(value);
      }
      
      return this;
   }

}
