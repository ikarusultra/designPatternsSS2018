package assignment6.model.util;

import org.sdmlib.models.pattern.PatternObject;
import assignment6.model.Group;
import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;
import assignment6.model.util.GroupPO;
import assignment6.model.util.GroupSet;
import assignment6.model.util.LinePO;
import assignment6.model.Line;
import assignment6.model.util.LineSet;

public class GroupPO extends PatternObject<GroupPO, Group>
{

    public GroupSet allMatches()
   {
      this.setDoAllMatches(true);
      
      GroupSet matches = new GroupSet();

      while (this.getPattern().getHasMatch())
      {
         matches.add((Group) this.getCurrentMatch());
         
         this.getPattern().findMatch();
      }
      
      return matches;
   }


   public GroupPO(){
      newInstance(null);
   }

   public GroupPO(Group... hostGraphObject) {
      if(hostGraphObject==null || hostGraphObject.length<1){
         return ;
      }
      newInstance(null, hostGraphObject);
   }

   public GroupPO(String modifier)
   {
      this.setModifier(modifier);
   }
   public GroupPO createNameCondition(String value)
   {
      new AttributeConstraint()
      .withAttrName(Group.PROPERTY_NAME)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public GroupPO createNameCondition(String lower, String upper)
   {
      new AttributeConstraint()
      .withAttrName(Group.PROPERTY_NAME)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public GroupPO createNameAssignment(String value)
   {
      new AttributeConstraint()
      .withAttrName(Group.PROPERTY_NAME)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public String getName()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Group) getCurrentMatch()).getName();
      }
      return null;
   }
   
   public GroupPO withName(String value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((Group) getCurrentMatch()).setName(value);
      }
      return this;
   }
   
   public GroupPO createParentGroupPO()
   {
      GroupPO result = new GroupPO(new Group[]{});
      
      result.setModifier(this.getPattern().getModifier());
      super.hasLink(Group.PROPERTY_PARENTGROUP, result);
      
      return result;
   }

   public GroupPO createParentGroupPO(String modifier)
   {
      GroupPO result = new GroupPO(new Group[]{});
      
      result.setModifier(modifier);
      super.hasLink(Group.PROPERTY_PARENTGROUP, result);
      
      return result;
   }

   public GroupPO createParentGroupLink(GroupPO tgt)
   {
      return hasLinkConstraint(tgt, Group.PROPERTY_PARENTGROUP);
   }

   public GroupPO createParentGroupLink(GroupPO tgt, String modifier)
   {
      return hasLinkConstraint(tgt, Group.PROPERTY_PARENTGROUP, modifier);
   }

   public Group getParentGroup()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Group) this.getCurrentMatch()).getParentGroup();
      }
      return null;
   }

   public GroupPO createGroupsPO()
   {
      GroupPO result = new GroupPO(new Group[]{});
      
      result.setModifier(this.getPattern().getModifier());
      super.hasLink(Group.PROPERTY_GROUPS, result);
      
      return result;
   }

   public GroupPO createGroupsPO(String modifier)
   {
      GroupPO result = new GroupPO(new Group[]{});
      
      result.setModifier(modifier);
      super.hasLink(Group.PROPERTY_GROUPS, result);
      
      return result;
   }

   public GroupPO createGroupsLink(GroupPO tgt)
   {
      return hasLinkConstraint(tgt, Group.PROPERTY_GROUPS);
   }

   public GroupPO createGroupsLink(GroupPO tgt, String modifier)
   {
      return hasLinkConstraint(tgt, Group.PROPERTY_GROUPS, modifier);
   }

   public GroupSet getGroups()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Group) this.getCurrentMatch()).getGroups();
      }
      return null;
   }

   public LinePO createLinesPO()
   {
      LinePO result = new LinePO(new Line[]{});
      
      result.setModifier(this.getPattern().getModifier());
      super.hasLink(Group.PROPERTY_LINES, result);
      
      return result;
   }

   public LinePO createLinesPO(String modifier)
   {
      LinePO result = new LinePO(new Line[]{});
      
      result.setModifier(modifier);
      super.hasLink(Group.PROPERTY_LINES, result);
      
      return result;
   }

   public GroupPO createLinesLink(LinePO tgt)
   {
      return hasLinkConstraint(tgt, Group.PROPERTY_LINES);
   }

   public GroupPO createLinesLink(LinePO tgt, String modifier)
   {
      return hasLinkConstraint(tgt, Group.PROPERTY_LINES, modifier);
   }

   public LineSet getLines()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Group) this.getCurrentMatch()).getLines();
      }
      return null;
   }

}
