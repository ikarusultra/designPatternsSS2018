package assignment6.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;
import de.uniks.networkparser.IdMap;
import assignment6.model.Line;

public class LinePOCreator extends PatternObjectCreator
{
   @Override
   public Object getSendableInstance(boolean reference)
   {
      if(reference) {
          return new LinePO(new Line[]{});
      } else {
          return new LinePO();
      }
   }
   
   public static IdMap createIdMap(String sessionID) {
      return assignment6.model.util.CreatorCreator.createIdMap(sessionID);
   }
}
