package assignment6.model.util;

import de.uniks.networkparser.IdMap;
import org.sdmlib.serialization.SDMLibJsonIdMap;

class CreatorCreator{

   public static IdMap createIdMap(String session)
   {
      IdMap jsonIdMap = new SDMLibJsonIdMap().withSession(session);
      jsonIdMap.with(new GroupCreator());
      jsonIdMap.with(new GroupPOCreator());
      jsonIdMap.with(new LineCreator());
      jsonIdMap.with(new LinePOCreator());
      return jsonIdMap;
   }
}
