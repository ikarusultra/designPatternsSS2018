/*
   Copyright (c) 2018 Ikarus
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package assignment6.model.util;

import de.uniks.networkparser.list.SimpleSet;
import assignment6.model.Line;
import de.uniks.networkparser.interfaces.Condition;
import java.util.Collection;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.list.NumberList;
import assignment6.model.util.GroupSet;
import assignment6.model.Group;

public class LineSet extends SimpleSet<Line>
{
	public Class<?> getTypClass() {
		return Line.class;
	}

   public LineSet()
   {
      // empty
   }

   public LineSet(Line... objects)
   {
      for (Line obj : objects)
      {
         this.add(obj);
      }
   }

   public LineSet(Collection<Line> objects)
   {
      this.addAll(objects);
   }

   public static final LineSet EMPTY_SET = new LineSet().withFlag(LineSet.READONLY);


   public LinePO createLinePO()
   {
      return new LinePO(this.toArray(new Line[this.size()]));
   }


   public String getEntryType()
   {
      return "assignment6.model.Line";
   }


   @Override
   public LineSet getNewList(boolean keyValue)
   {
      return new LineSet();
   }


   @SuppressWarnings("unchecked")
   public LineSet with(Object value)
   {
      if (value == null)
      {
         return this;
      }
      else if (value instanceof java.util.Collection)
      {
         this.addAll((Collection<Line>)value);
      }
      else if (value != null)
      {
         this.add((Line) value);
      }
      
      return this;
   }
   
   public LineSet without(Line value)
   {
      this.remove(value);
      return this;
   }


   /**
    * Loop through the current set of Line objects and collect a list of the name attribute values. 
    * 
    * @return List of String objects reachable via name attribute
    */
   public ObjectSet getName()
   {
      ObjectSet result = new ObjectSet();
      
      for (Line obj : this)
      {
         result.add(obj.getName());
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and collect those Line objects where the name attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of Line objects that match the parameter
    */
   public LineSet createNameCondition(String value)
   {
      LineSet result = new LineSet();
      
      for (Line obj : this)
      {
         if (value.equals(obj.getName()))
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and collect those Line objects where the name attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of Line objects that match the parameter
    */
   public LineSet createNameCondition(String lower, String upper)
   {
      LineSet result = new LineSet();
      
      for (Line obj : this)
      {
         if (lower.compareTo(obj.getName()) <= 0 && obj.getName().compareTo(upper) <= 0)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and assign value to the name attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of Line objects now with new attribute values.
    */
   public LineSet withName(String value)
   {
      for (Line obj : this)
      {
         obj.setName(value);
      }
      
      return this;
   }


   /**
    * Loop through the current set of Line objects and collect a list of the x1 attribute values. 
    * 
    * @return List of int objects reachable via x1 attribute
    */
   public NumberList getX1()
   {
      NumberList result = new NumberList();
      
      for (Line obj : this)
      {
         result.add(obj.getX1());
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and collect those Line objects where the x1 attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of Line objects that match the parameter
    */
   public LineSet createX1Condition(int value)
   {
      LineSet result = new LineSet();
      
      for (Line obj : this)
      {
         if (value == obj.getX1())
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and collect those Line objects where the x1 attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of Line objects that match the parameter
    */
   public LineSet createX1Condition(int lower, int upper)
   {
      LineSet result = new LineSet();
      
      for (Line obj : this)
      {
         if (lower <= obj.getX1() && obj.getX1() <= upper)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and assign value to the x1 attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of Line objects now with new attribute values.
    */
   public LineSet withX1(int value)
   {
      for (Line obj : this)
      {
         obj.setX1(value);
      }
      
      return this;
   }


   /**
    * Loop through the current set of Line objects and collect a list of the x2 attribute values. 
    * 
    * @return List of int objects reachable via x2 attribute
    */
   public NumberList getX2()
   {
      NumberList result = new NumberList();
      
      for (Line obj : this)
      {
         result.add(obj.getX2());
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and collect those Line objects where the x2 attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of Line objects that match the parameter
    */
   public LineSet createX2Condition(int value)
   {
      LineSet result = new LineSet();
      
      for (Line obj : this)
      {
         if (value == obj.getX2())
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and collect those Line objects where the x2 attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of Line objects that match the parameter
    */
   public LineSet createX2Condition(int lower, int upper)
   {
      LineSet result = new LineSet();
      
      for (Line obj : this)
      {
         if (lower <= obj.getX2() && obj.getX2() <= upper)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and assign value to the x2 attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of Line objects now with new attribute values.
    */
   public LineSet withX2(int value)
   {
      for (Line obj : this)
      {
         obj.setX2(value);
      }
      
      return this;
   }


   /**
    * Loop through the current set of Line objects and collect a list of the y1 attribute values. 
    * 
    * @return List of int objects reachable via y1 attribute
    */
   public NumberList getY1()
   {
      NumberList result = new NumberList();
      
      for (Line obj : this)
      {
         result.add(obj.getY1());
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and collect those Line objects where the y1 attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of Line objects that match the parameter
    */
   public LineSet createY1Condition(int value)
   {
      LineSet result = new LineSet();
      
      for (Line obj : this)
      {
         if (value == obj.getY1())
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and collect those Line objects where the y1 attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of Line objects that match the parameter
    */
   public LineSet createY1Condition(int lower, int upper)
   {
      LineSet result = new LineSet();
      
      for (Line obj : this)
      {
         if (lower <= obj.getY1() && obj.getY1() <= upper)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and assign value to the y1 attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of Line objects now with new attribute values.
    */
   public LineSet withY1(int value)
   {
      for (Line obj : this)
      {
         obj.setY1(value);
      }
      
      return this;
   }


   /**
    * Loop through the current set of Line objects and collect a list of the y2 attribute values. 
    * 
    * @return List of int objects reachable via y2 attribute
    */
   public NumberList getY2()
   {
      NumberList result = new NumberList();
      
      for (Line obj : this)
      {
         result.add(obj.getY2());
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and collect those Line objects where the y2 attribute matches the parameter value. 
    * 
    * @param value Search value
    * 
    * @return Subset of Line objects that match the parameter
    */
   public LineSet createY2Condition(int value)
   {
      LineSet result = new LineSet();
      
      for (Line obj : this)
      {
         if (value == obj.getY2())
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and collect those Line objects where the y2 attribute is between lower and upper. 
    * 
    * @param lower Lower bound 
    * @param upper Upper bound 
    * 
    * @return Subset of Line objects that match the parameter
    */
   public LineSet createY2Condition(int lower, int upper)
   {
      LineSet result = new LineSet();
      
      for (Line obj : this)
      {
         if (lower <= obj.getY2() && obj.getY2() <= upper)
         {
            result.add(obj);
         }
      }
      
      return result;
   }


   /**
    * Loop through the current set of Line objects and assign value to the y2 attribute of each of it. 
    * 
    * @param value New attribute value
    * 
    * @return Current set of Line objects now with new attribute values.
    */
   public LineSet withY2(int value)
   {
      for (Line obj : this)
      {
         obj.setY2(value);
      }
      
      return this;
   }

   /**
    * Loop through the current set of Line objects and collect a set of the Group objects reached via group. 
    * 
    * @return Set of Group objects reachable via group
    */
   public GroupSet getGroup()
   {
      GroupSet result = new GroupSet();
      
      for (Line obj : this)
      {
         result.with(obj.getGroup());
      }
      
      return result;
   }

   /**
    * Loop through the current set of Line objects and collect all contained objects with reference group pointing to the object passed as parameter. 
    * 
    * @param value The object required as group neighbor of the collected results. 
    * 
    * @return Set of Group objects referring to value via group
    */
   public LineSet filterGroup(Object value)
   {
      ObjectSet neighbors = new ObjectSet();

      if (value instanceof Collection)
      {
         neighbors.addAll((Collection<?>) value);
      }
      else
      {
         neighbors.add(value);
      }
      
      LineSet answer = new LineSet();
      
      for (Line obj : this)
      {
         if (neighbors.contains(obj.getGroup()) || (neighbors.isEmpty() && obj.getGroup() == null))
         {
            answer.add(obj);
         }
      }
      
      return answer;
   }

   /**
    * Loop through current set of ModelType objects and attach the Line object passed as parameter to the Group attribute of each of it. 
    * 
    * @param value value    * @return The original set of ModelType objects now with the new neighbor attached to their Group attributes.
    */
   public LineSet withGroup(Group value)
   {
      for (Line obj : this)
      {
         obj.withGroup(value);
      }
      
      return this;
   }

}
