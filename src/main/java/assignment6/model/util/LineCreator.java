/*
   Copyright (c) 2018 Ikarus
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package assignment6.model.util;

import de.uniks.networkparser.interfaces.AggregatedEntityCreator;
import assignment6.model.Line;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.IdMap;
import assignment6.model.Group;

public class LineCreator implements AggregatedEntityCreator
{
   public static final LineCreator it = new LineCreator();
   
   private final String[] properties = new String[]
   {
      Line.PROPERTY_NAME,
      Line.PROPERTY_X1,
      Line.PROPERTY_X2,
      Line.PROPERTY_Y1,
      Line.PROPERTY_Y2,
      Line.PROPERTY_GROUP,
   };
   
   private final String[] upProperties = new String[]
   {
   };
   
   private final String[] downProperties = new String[]
   {
   };
   
   @Override
   public String[] getProperties()
   {
      return properties;
   }
   
   @Override
   public String[] getUpProperties()
   {
      return upProperties;
   }
   
   @Override
   public String[] getDownProperties()
   {
      return downProperties;
   }
   
   @Override
   public Object getSendableInstance(boolean reference)
   {
      return new Line();
   }
   
   
   @Override
   public Object getValue(Object target, String attrName)
   {
      int pos = attrName.indexOf('.');
      String attribute = attrName;
      
      if (pos > 0)
      {
         attribute = attrName.substring(0, pos);
      }

      if (Line.PROPERTY_NAME.equalsIgnoreCase(attribute))
      {
         return ((Line) target).getName();
      }

      if (Line.PROPERTY_X1.equalsIgnoreCase(attribute))
      {
         return ((Line) target).getX1();
      }

      if (Line.PROPERTY_X2.equalsIgnoreCase(attribute))
      {
         return ((Line) target).getX2();
      }

      if (Line.PROPERTY_Y1.equalsIgnoreCase(attribute))
      {
         return ((Line) target).getY1();
      }

      if (Line.PROPERTY_Y2.equalsIgnoreCase(attribute))
      {
         return ((Line) target).getY2();
      }

      if (Line.PROPERTY_GROUP.equalsIgnoreCase(attribute))
      {
         return ((Line) target).getGroup();
      }
      
      return null;
   }
   
   @Override
   public boolean setValue(Object target, String attrName, Object value, String type)
   {
      if (Line.PROPERTY_Y2.equalsIgnoreCase(attrName))
      {
         ((Line) target).setY2(Integer.parseInt(value.toString()));
         return true;
      }

      if (Line.PROPERTY_Y1.equalsIgnoreCase(attrName))
      {
         ((Line) target).setY1(Integer.parseInt(value.toString()));
         return true;
      }

      if (Line.PROPERTY_X2.equalsIgnoreCase(attrName))
      {
         ((Line) target).setX2(Integer.parseInt(value.toString()));
         return true;
      }

      if (Line.PROPERTY_X1.equalsIgnoreCase(attrName))
      {
         ((Line) target).setX1(Integer.parseInt(value.toString()));
         return true;
      }

      if (Line.PROPERTY_NAME.equalsIgnoreCase(attrName))
      {
         ((Line) target).setName((String) value);
         return true;
      }

      if(SendableEntityCreator.REMOVE_YOU.equals(type)) {
           ((Line)target).removeYou();
           return true;
      }
      if (SendableEntityCreator.REMOVE.equals(type) && value != null)
      {
         attrName = attrName + type;
      }

      if (Line.PROPERTY_GROUP.equalsIgnoreCase(attrName))
      {
         ((Line) target).setGroup((Group) value);
         return true;
      }
      
      return false;
   }
   public static IdMap createIdMap(String sessionID)
   {
      return assignment6.model.util.CreatorCreator.createIdMap(sessionID);
   }
   
   //==========================================================================
      public void removeObject(Object entity)
   {
      ((Line) entity).removeYou();
   }
}
