package assignment6.model.util;

import org.sdmlib.models.pattern.PatternObject;
import assignment6.model.Line;
import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;
import assignment6.model.util.GroupPO;
import assignment6.model.Group;
import assignment6.model.util.LinePO;

public class LinePO extends PatternObject<LinePO, Line>
{

    public LineSet allMatches()
   {
      this.setDoAllMatches(true);
      
      LineSet matches = new LineSet();

      while (this.getPattern().getHasMatch())
      {
         matches.add((Line) this.getCurrentMatch());
         
         this.getPattern().findMatch();
      }
      
      return matches;
   }


   public LinePO(){
      newInstance(null);
   }

   public LinePO(Line... hostGraphObject) {
      if(hostGraphObject==null || hostGraphObject.length<1){
         return ;
      }
      newInstance(null, hostGraphObject);
   }

   public LinePO(String modifier)
   {
      this.setModifier(modifier);
   }
   public LinePO createNameCondition(String value)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_NAME)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public LinePO createNameCondition(String lower, String upper)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_NAME)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public LinePO createNameAssignment(String value)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_NAME)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public String getName()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Line) getCurrentMatch()).getName();
      }
      return null;
   }
   
   public LinePO withName(String value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((Line) getCurrentMatch()).setName(value);
      }
      return this;
   }
   
   public LinePO createX1Condition(int value)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_X1)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public LinePO createX1Condition(int lower, int upper)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_X1)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public LinePO createX1Assignment(int value)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_X1)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public int getX1()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Line) getCurrentMatch()).getX1();
      }
      return 0;
   }
   
   public LinePO withX1(int value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((Line) getCurrentMatch()).setX1(value);
      }
      return this;
   }
   
   public LinePO createX2Condition(int value)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_X2)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public LinePO createX2Condition(int lower, int upper)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_X2)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public LinePO createX2Assignment(int value)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_X2)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public int getX2()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Line) getCurrentMatch()).getX2();
      }
      return 0;
   }
   
   public LinePO withX2(int value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((Line) getCurrentMatch()).setX2(value);
      }
      return this;
   }
   
   public LinePO createY1Condition(int value)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_Y1)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public LinePO createY1Condition(int lower, int upper)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_Y1)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public LinePO createY1Assignment(int value)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_Y1)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public int getY1()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Line) getCurrentMatch()).getY1();
      }
      return 0;
   }
   
   public LinePO withY1(int value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((Line) getCurrentMatch()).setY1(value);
      }
      return this;
   }
   
   public LinePO createY2Condition(int value)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_Y2)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public LinePO createY2Condition(int lower, int upper)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_Y2)
      .withTgtValue(lower)
      .withUpperTgtValue(upper)
      .withSrc(this)
      .withModifier(this.getPattern().getModifier())
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public LinePO createY2Assignment(int value)
   {
      new AttributeConstraint()
      .withAttrName(Line.PROPERTY_Y2)
      .withTgtValue(value)
      .withSrc(this)
      .withModifier(Pattern.CREATE)
      .withPattern(this.getPattern());
      
      super.filterAttr();
      
      return this;
   }
   
   public int getY2()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Line) getCurrentMatch()).getY2();
      }
      return 0;
   }
   
   public LinePO withY2(int value)
   {
      if (this.getPattern().getHasMatch())
      {
         ((Line) getCurrentMatch()).setY2(value);
      }
      return this;
   }
   
   public GroupPO createGroupPO()
   {
      GroupPO result = new GroupPO(new Group[]{});
      
      result.setModifier(this.getPattern().getModifier());
      super.hasLink(Line.PROPERTY_GROUP, result);
      
      return result;
   }

   public GroupPO createGroupPO(String modifier)
   {
      GroupPO result = new GroupPO(new Group[]{});
      
      result.setModifier(modifier);
      super.hasLink(Line.PROPERTY_GROUP, result);
      
      return result;
   }

   public LinePO createGroupLink(GroupPO tgt)
   {
      return hasLinkConstraint(tgt, Line.PROPERTY_GROUP);
   }

   public LinePO createGroupLink(GroupPO tgt, String modifier)
   {
      return hasLinkConstraint(tgt, Line.PROPERTY_GROUP, modifier);
   }

   public Group getGroup()
   {
      if (this.getPattern().getHasMatch())
      {
         return ((Line) this.getCurrentMatch()).getGroup();
      }
      return null;
   }

}
