package assignment6.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;
import de.uniks.networkparser.IdMap;
import assignment6.model.Group;

public class GroupPOCreator extends PatternObjectCreator
{
   @Override
   public Object getSendableInstance(boolean reference)
   {
      if(reference) {
          return new GroupPO(new Group[]{});
      } else {
          return new GroupPO();
      }
   }
   
   public static IdMap createIdMap(String sessionID) {
      return assignment6.model.util.CreatorCreator.createIdMap(sessionID);
   }
}
