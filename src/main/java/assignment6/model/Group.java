/*
   Copyright (c) 2018 Ikarus
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
   
package assignment6.model;

import de.uniks.networkparser.interfaces.SendableEntity;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyChangeListener;
import de.uniks.networkparser.EntityUtil;
import assignment6.model.util.GroupSet;
import assignment6.model.util.LineSet;
import assignment6.model.Line;

public  class Group implements SendableEntity
{

   
   //==========================================================================
   
   protected PropertyChangeSupport listeners = null;
   
   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null) {
   		listeners.firePropertyChange(propertyName, oldValue, newValue);
   		return true;
   	}
   	return false;
   }
   
   public boolean addPropertyChangeListener(PropertyChangeListener listener) 
   {
   	if (listeners == null) {
   		listeners = new PropertyChangeSupport(this);
   	}
   	listeners.addPropertyChangeListener(listener);
   	return true;
   }
   
   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
   	if (listeners == null) {
   		listeners = new PropertyChangeSupport(this);
   	}
   	listeners.addPropertyChangeListener(propertyName, listener);
   	return true;
   }
   
   public boolean removePropertyChangeListener(PropertyChangeListener listener) {
   	if (listeners != null) {
   		listeners.removePropertyChangeListener(listener);
   	}
   	return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener) {
   	if (listeners != null) {
   		listeners.removePropertyChangeListener(propertyName, listener);
   	}
   	return true;
   }

   
   //==========================================================================
   
   
   public void removeYou()
   {
      setParentGroup(null);
      withoutGroups(this.getGroups().toArray(new Group[this.getGroups().size()]));
      withoutLines(this.getLines().toArray(new Line[this.getLines().size()]));
      firePropertyChange("REMOVE_YOU", this, null);
   }

   
   //==========================================================================
   
   public static final String PROPERTY_NAME = "name";
   
   private String name;

   public String getName()
   {
      return this.name;
   }
   
   public void setName(String value)
   {
      if ( ! EntityUtil.stringEquals(this.name, value)) {
      
         String oldValue = this.name;
         this.name = value;
         this.firePropertyChange(PROPERTY_NAME, oldValue, value);
      }
   }
   
   public Group withName(String value)
   {
      setName(value);
      return this;
   } 


   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();
      
      result.append(" ").append(this.getName());
      return result.substring(1);
   }


   
   /********************************************************************
    * <pre>
    *              many                       one
    * Group ----------------------------------- Group
    *              groups                   parentGroup
    * </pre>
    */
   
   public static final String PROPERTY_PARENTGROUP = "parentGroup";

   private Group parentGroup = null;

   public Group getParentGroup()
   {
      return this.parentGroup;
   }
   public GroupSet getParentGroupTransitive()
   {
      GroupSet result = new GroupSet().with(this);
      return result.getParentGroupTransitive();
   }


   public boolean setParentGroup(Group value)
   {
      boolean changed = false;
      
      if (this.parentGroup != value)
      {
         Group oldValue = this.parentGroup;
         
         if (this.parentGroup != null)
         {
            this.parentGroup = null;
            oldValue.withoutGroups(this);
         }
         
         this.parentGroup = value;
         
         if (value != null)
         {
            value.withGroups(this);
         }
         
         firePropertyChange(PROPERTY_PARENTGROUP, oldValue, value);
         changed = true;
      }
      
      return changed;
   }

   public Group withParentGroup(Group value)
   {
      setParentGroup(value);
      return this;
   } 

   public Group createParentGroup()
   {
      Group value = new Group();
      withParentGroup(value);
      return value;
   } 

   
   /********************************************************************
    * <pre>
    *              one                       many
    * Group ----------------------------------- Group
    *              parentGroup                   groups
    * </pre>
    */
   
   public static final String PROPERTY_GROUPS = "groups";

   private GroupSet groups = null;
   
   public GroupSet getGroups()
   {
      if (this.groups == null)
      {
         return GroupSet.EMPTY_SET;
      }
   
      return this.groups;
   }
   public GroupSet getGroupsTransitive()
   {
      GroupSet result = new GroupSet().with(this);
      return result.getGroupsTransitive();
   }


   public Group withGroups(Group... value)
   {
      if(value==null){
         return this;
      }
      for (Group item : value)
      {
         if (item != null)
         {
            if (this.groups == null)
            {
               this.groups = new GroupSet();
            }
            
            boolean changed = this.groups.add (item);

            if (changed)
            {
               item.withParentGroup(this);
               firePropertyChange(PROPERTY_GROUPS, null, item);
            }
         }
      }
      return this;
   } 

   public Group withoutGroups(Group... value)
   {
      for (Group item : value)
      {
         if ((this.groups != null) && (item != null))
         {
            if (this.groups.remove(item))
            {
               item.setParentGroup(null);
               firePropertyChange(PROPERTY_GROUPS, item, null);
            }
         }
      }
      return this;
   }

   public Group createGroups()
   {
      Group value = new Group();
      withGroups(value);
      return value;
   } 

   
   /********************************************************************
    * <pre>
    *              one                       many
    * Group ----------------------------------- Line
    *              group                   lines
    * </pre>
    */
   
   public static final String PROPERTY_LINES = "lines";

   private LineSet lines = null;
   
   public LineSet getLines()
   {
      if (this.lines == null)
      {
         return LineSet.EMPTY_SET;
      }
   
      return this.lines;
   }

   public Group withLines(Line... value)
   {
      if(value==null){
         return this;
      }
      for (Line item : value)
      {
         if (item != null)
         {
            if (this.lines == null)
            {
               this.lines = new LineSet();
            }
            
            boolean changed = this.lines.add (item);

            if (changed)
            {
               item.withGroup(this);
               firePropertyChange(PROPERTY_LINES, null, item);
            }
         }
      }
      return this;
   } 

   public Group withoutLines(Line... value)
   {
      for (Line item : value)
      {
         if ((this.lines != null) && (item != null))
         {
            if (this.lines.remove(item))
            {
               item.setGroup(null);
               firePropertyChange(PROPERTY_LINES, item, null);
            }
         }
      }
      return this;
   }

   public Line createLines()
   {
      Line value = new Line();
      withLines(value);
      return value;
   } 
}
