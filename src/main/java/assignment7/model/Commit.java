package assignment7.model;

import com.google.gson.Gson;

public class Commit {

    public Commit(String gitUrl, String message, String author, String commitTime, String crawlTime) {
        this.gitUrl = gitUrl;
        this.message = message;
        this.author = author;
        this.commitTime = commitTime;
        this.crawlTime = crawlTime;
    }

    String gitUrl;

    String message;

    String author;

    String commitTime;

    String crawlTime;


    public String toJson() {
        return new Gson().toJson(this);
    }

    public String getGitUrl() {
        return gitUrl;
    }

    public void setGitUrl(String gitUrl) {
        this.gitUrl = gitUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(String commitTime) {
        this.commitTime = commitTime;
    }

    public String getCrawlTime() {
        return crawlTime;
    }

    public void setCrawlTime(String crawlTime) {
        this.crawlTime = crawlTime;
    }

    @Override
    public String toString(){
        StringBuilder buildder = new StringBuilder();
        buildder.append("Url: ").append(gitUrl).append("\n");
        buildder.append("Commit message: ").append(message).append("\n");
        buildder.append("Author: ").append(author).append("\n");
        buildder.append("Commit time: ").append(commitTime).append("\n");
        buildder.append("Crawl time: ").append(crawlTime).append("\n");

        return buildder.toString();
    }

}
