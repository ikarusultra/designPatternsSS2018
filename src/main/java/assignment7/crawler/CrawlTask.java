package assignment7.crawler;

import assignment7.model.Commit;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;


public class CrawlTask implements Runnable {

    private String lastCrawledCommitId = "NotYetCrawled";

    private final CrawlerProxy proxy;

    public CrawlTask() {
        proxy = new CrawlerProxy();
    }

    @Override
    public void run() {
        Document doc = null;
        System.out.println();
        System.out.println("Checking for new commits...");
        String baseUrl = "https://gitlab.com/ikarusultra/designPatternsSS2018";
        try {
            doc = Jsoup.connect(baseUrl + "/commits/master").timeout(30*1000).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements commitElements = doc.select("li[id*=commit]");
        Element lastCommitElement = commitElements.first();
        String lastCommitId = lastCommitElement.select("a[href*=/commit/]").attr("href");

        lastCommitId = lastCommitId.split("/commit/")[1];

        if(!lastCommitId.equals(lastCrawledCommitId)){
            System.out.println("New commit found: " + lastCommitId);
            lastCrawledCommitId = lastCommitId;

            String commitUrl = baseUrl + "/commit/" + lastCommitId;
            try {
                doc = Jsoup.connect(commitUrl).timeout(30*1000).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            LocalDateTime localDateTime = LocalDateTime.now(ZoneOffset.UTC);
            String localDate = localDateTime.toLocalDate().toString();
            String localTime = localDateTime.toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"));

            String commitMessage = doc.select("h3[class=commit-title]").text();
            String commitAuthor = doc.select("span[class=commit-author-name]").text();
            String commitTime = doc.select("time[data-toggle=tooltip]").attr("datetime");
            String crawlTime = localDate + "T" + localTime + "Z";

            Commit newestCommit = new Commit(commitUrl, commitMessage , commitAuthor, commitTime, crawlTime);
            System.out.println();
            System.out.println(newestCommit);
            proxy.notifyCommit(newestCommit);
        }else{
            System.out.println("No new commit was found since last check.");
            System.out.println("Newest Commit id: " + lastCommitId);
        }
        System.out.println("-------------------------------------------------------------------------------------------" +
                "-----------------------------------------------------");

    }
}
