package assignment7.crawler;


import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

// https://gitlab.com/ikarusultra/designPatternsSS2018/commits/master
public class Crawler {

    private static ScheduledExecutorService scheduledExecutorService;

    public void startCrawling(){

        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(new CrawlTask(), 0,30, TimeUnit.SECONDS);

    }


}
