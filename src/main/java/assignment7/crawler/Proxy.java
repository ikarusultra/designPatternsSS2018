package assignment7.crawler;
import assignment7.model.Commit;

interface Proxy {

    void notifyCommit(Commit commit);

}
