package assignment7.crawler;

import assignment7.Constants;
import assignment7.model.Commit;
import com.google.gson.Gson;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class CrawlerProxy implements MqttCallback, Proxy {

    private MqttClient client;
    private Gson gsonParser = new Gson();

    @Override
    public void notifyCommit(Commit commit) {
        if (client == null || !client.isConnected()) {
            connect();
        }

        String messageContent = gsonParser.toJson(commit);
        int qos = 2;

        System.out.println("Message: " + messageContent);
        MqttMessage message = new MqttMessage(messageContent.getBytes());
        message.setQos(qos);

        try {
            client.publish(Constants.TOPIC, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void connect() {
        String clientId = System.currentTimeMillis() + "33203230";
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            client = new MqttClient(Constants.BROKER, clientId, persistence);
            client.setCallback(this);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println("Connecting to MQTT server: " + Constants.BROKER);
            client.connect(connOpts);


        } catch (MqttException exception) {
            System.out.println("Exception: " + exception);
            exception.printStackTrace();
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage message){}

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {}

    @Override
    public void connectionLost(Throwable cause) {}


}
