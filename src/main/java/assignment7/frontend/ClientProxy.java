package assignment7.frontend;

import assignment7.Constants;
import assignment7.model.Commit;
import com.google.gson.Gson;
import javafx.application.Platform;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.nio.charset.StandardCharsets;

public class ClientProxy implements MqttCallback {

    private final Callback callback;
    private Gson gsonParser = new Gson();

    ClientProxy(Callback callback) {
        this.callback = callback;
        connectClient();
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        String str = new String(message.getPayload(), StandardCharsets.UTF_8);
        System.out.println("-----------------------------------------------------");
        System.out.println("Json String : " + str);
        System.out.println("-----------------------------------------------------");

        Commit commit = gsonParser.fromJson(str, Commit.class);

        Platform.runLater(() -> callback.receiveCommit(commit));
    }

    private void connectClient() {
        String clientId = System.currentTimeMillis() + "33203230";
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            MqttClient sampleClient = new MqttClient(Constants.BROKER, clientId, persistence);
            sampleClient.setCallback(this);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println("Connecting to broker: " + Constants.BROKER);
            sampleClient.connect(connOpts);
            System.out.println("Connected");

            sampleClient.subscribe(Constants.TOPIC);

        } catch (MqttException e) {
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
    }


    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        System.out.println("Delivery complete");
    }

    @Override
    public void connectionLost(Throwable cause) {
        System.out.println("Connection lost");
    }
}
