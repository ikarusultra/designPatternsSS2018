package assignment7.frontend;

import assignment7.model.Commit;

public interface Callback {
    void receiveCommit(Commit commit);
}
