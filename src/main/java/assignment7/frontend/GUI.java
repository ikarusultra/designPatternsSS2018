package assignment7.frontend;

import assignment7.model.Commit;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GUI implements Callback {

    private final VBox label;

    GUI(Stage primaryStage) {
        new ClientProxy(this);
        primaryStage.setTitle("Commit List");

        ScrollPane scrollPane = new ScrollPane();

        label = new VBox();
        label.setSpacing(10);
        label.setPadding(new Insets(10,10,10,10));

        scrollPane.setContent(label);
        primaryStage.setScene(new Scene(scrollPane, 550, 700));
        primaryStage.show();
    }

    private void updateCanvas(Commit commit) {

        Label commitLabel = new Label(commit.toString());
        //BorderPane.setMargin(commitLabel, new Insets(20, 20, 20, 20));

        label.getChildren().add(commitLabel);
        label.getChildren().add(new Label("--------------------------------------------------------------" +
                "---------------------------------------------"));
    }

    @Override
    public void receiveCommit(Commit commit) {
        updateCanvas(commit);
    }
}
