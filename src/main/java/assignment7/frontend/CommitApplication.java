package assignment7.frontend;
import javafx.application.Application;
import javafx.stage.Stage;

public class CommitApplication extends Application {

    // Run gitlab crawler with:
    // docker run -d ikarusultra/crawler

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        new GUI(primaryStage);
    }
}
