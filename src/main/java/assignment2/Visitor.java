package assignment2;

public interface Visitor {
    void visit(Fachgebiet fachgebiet);
    void visit(Node node);

}
