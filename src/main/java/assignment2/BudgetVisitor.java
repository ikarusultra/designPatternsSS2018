package assignment2;

public class BudgetVisitor implements Visitor{

    int currentBudget = 0;

    @Override
    public void visit(Fachgebiet currentFachgebiet) {
        currentBudget += currentFachgebiet.getBudget();
        System.out.println("Visited: " + currentFachgebiet.toString() + " | Current budget: " + currentBudget);
    }

    @Override
    public void visit(Node node) {

        for (Node child : node.getKids()) {
           child.accept(this);
        }

    }

    public void setCurrentBudget(int currentBudget) {
        this.currentBudget = currentBudget;
    }

    public int getCurrentBudget() {
        return currentBudget;
    }
}
