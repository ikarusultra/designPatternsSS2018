package assignment2;

import java.util.ArrayList;

public class Node {
    ArrayList<Node> kids = new ArrayList<>();

    public void accept(Visitor visitor){
        visitor.visit(this);
    }

    public ArrayList<Node> getKids() {
        return kids;
    }

    public void addChild(Node childNode) {
        kids.add(childNode);
    }

}
