package assignment2;

import java.util.ArrayList;

public class Fachgebiet extends Node{ //Professor, Doktoranten


    public Fachgebiet(int budget) {
        this.budget = budget;
    }

    public void accept(Visitor visitor){
        visitor.visit(this);
    }

    public ArrayList<Node> getEmployees() {
        return employees;
    }

    public void setPersons(ArrayList<Node> persons) {
        this.employees = persons;
    }

    ArrayList<Node> employees = new ArrayList<Node>();

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    int budget;
}
