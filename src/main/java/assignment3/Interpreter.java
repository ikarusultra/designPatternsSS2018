package assignment3;

import java.util.*;

public class Interpreter {

    private Stack<Integer> stack;
    private HashMap<String, Integer> symbolTable;

    public int getStackSize() {
        return stack.size();
    }

    public int getTopStackElement() {
        return stack.peek();
    }

    private ArrayList<CommandHandler> handlers;

    public Interpreter(){
        stack = new Stack<>();
        symbolTable = new HashMap<>(); //Stores variables
        handlers = new ArrayList<>();  // Holds all registered Handlers

        handlers.add(new LoadConstantCommandHandler());
        handlers.add(new MultCommandHandler());
        handlers.add(new StoreCommandHandler());
        handlers.add(new LoadVarCommandHandler());
        handlers.add(new PrintlnCommandHandler());
    }

    public void parse(String text){
        ArrayList<String> commands ;

        commands =  new ArrayList<>(Arrays.asList( text.split("\n")));

        for (String command : commands){ // Iterate over the commands
            handleCommand(command);
        }
    }

    private void handleCommand(String command) {
        boolean handled = false;

        for (CommandHandler handler : handlers){       // Iterate over the chain
            handled = handler.handle(command, stack, symbolTable);
            if (handled){
                return;
            }
        }

        System.err.println("No handler found for command: " + command);
    }

}
