package assignment3;

import java.util.HashMap;
import java.util.Stack;

public class MultCommandHandler implements CommandHandler{
    @Override
    public boolean handle(String command, Stack<Integer> stack, HashMap<String, Integer> symbolTable) {
        boolean handled = false;

        if("Mult".equals(command)){
            int factorA,factorB;
            factorA = stack.pop();
            factorB = stack.pop();
            stack.push(factorA * factorB);
            handled = true;
        }

        return handled;
    }
}
