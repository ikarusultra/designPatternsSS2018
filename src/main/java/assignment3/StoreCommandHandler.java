package assignment3;

import java.util.HashMap;
import java.util.Stack;

public class StoreCommandHandler implements CommandHandler{
    @Override
    public boolean handle(String command, Stack<Integer> stack, HashMap<String, Integer> symbolTable) {
        boolean handled = false;

        if(command.startsWith("Store ")){
            String varName = command.split("Store ")[1];
            symbolTable.put(varName, stack.pop());
            handled = true;
        }

        return handled;
    }
}
