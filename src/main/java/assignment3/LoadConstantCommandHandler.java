package assignment3;

import java.util.HashMap;
import java.util.Stack;

public class LoadConstantCommandHandler implements CommandHandler{
    @Override
    public boolean handle(String command, Stack<Integer> stack, HashMap<String, Integer>symbolTable) {
        boolean handled = false;
        if(command.startsWith("Ldc ")){
            String valueAsString = command.split("Ldc ")[1];
            try {
                int value = Integer.parseInt(valueAsString);
                stack.push(value);
            } catch (NumberFormatException e) {
                System.err.println("Can not load constant: " + valueAsString + ". Nothing was pushed on stack.");
            }
            handled = true;
        }
        return handled;
    }
}
