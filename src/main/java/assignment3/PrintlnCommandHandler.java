package assignment3;

import java.util.HashMap;
import java.util.Stack;

public class PrintlnCommandHandler implements CommandHandler{
    @Override
    public boolean handle(String command, Stack<Integer> stack , HashMap<String, Integer> symbolTable) {
        boolean handled = false;
        if("Print".equals(command)){
            System.out.println(stack.pop());
            handled = true;
        }
        return handled;
    }
}
