package assignment3;

import java.util.HashMap;
import java.util.Stack;

public interface CommandHandler {
    boolean handle(String command, Stack<Integer> stack, HashMap<String,Integer> symbolTable);
}

