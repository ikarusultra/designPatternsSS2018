package assignment3;

import java.util.HashMap;
import java.util.Stack;

public class LoadVarCommandHandler implements CommandHandler{
    @Override
    public boolean handle(String command, Stack<Integer> stack, HashMap<String, Integer> symbolTable) {
        boolean handled = false;
        if(command.startsWith("Ld ")){
            String varName = command.split("Ld ")[1];
            int value = symbolTable.get(varName);
            stack.push(value);
            handled = true;
        }
        return handled;
    }
}
