package assignment1;

public class BurgerBude extends Caterer{
    public void deliver(int foodNo, String address){
        System.out.println("All right, we will send the burger " + foodNo + " to address " + address + "!");
    }
}
