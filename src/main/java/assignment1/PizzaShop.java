package assignment1;

public class PizzaShop extends Caterer{
    public void deliver(int foodNo, String address){
        System.out.println("All right, we will send the pizza " + foodNo + " to address " + address + "!");
    }
}
