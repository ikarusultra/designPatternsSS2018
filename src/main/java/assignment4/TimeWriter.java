package assignment4;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeWriter implements Runnable{
    @Override
    public void run() {
        Date date;
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy : hh:mm:ss");

        while (true) {
            date = new Date(System.currentTimeMillis());
            try {
                Files.write(Paths.get("./src/main/resources/assignment4/time.txt"),
                        formatter.format(date).getBytes(StandardCharsets.UTF_8.name()));
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                break;
            }
            if (Thread.currentThread().isInterrupted()) {
                break;
            }
        }
    }
}
