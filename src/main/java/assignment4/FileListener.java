package assignment4;

import java.io.IOException;
import java.nio.file.*;

public class FileListener implements Runnable {

    int catchedPropertyChanges;

    public int getCatchedPropertyChanges() {
        return catchedPropertyChanges;
    }

    @Override
    public void run() {
        while (true) {

            try {
                WatchService watchService = FileSystems.getDefault().newWatchService();
                Path path = Paths.get("./src/main/resources/assignment4");

                path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);

                WatchKey key = watchService.take();

                for (WatchEvent<?> event : key.pollEvents()) {

                    path.resolve(java.lang.String.valueOf(event.context()));
                    if (String.valueOf(event.context()).equals("time.txt")){
                        System.out.println(new String(Files.readAllBytes(Paths.get(path + "/time.txt"))));
                        catchedPropertyChanges++;
                    }
                    key.reset();
                }
            } catch (IOException | InterruptedException e) {
                break;
            }

            if (Thread.currentThread().isInterrupted()) {
                break;
            }
        }
    }
}
