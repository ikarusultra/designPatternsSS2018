## Student

Name: Niclas Wolff <br />
MatNo: 33203230 <br />
Term: SS2018

## Synopsis

**Gradle wrapper** was used for build automation. 

## Motivation

Convention over Configuration!

## Installation

Import as gradle project. That should be it.

## CompilerTests

Please run them with ./gradlew test. 



